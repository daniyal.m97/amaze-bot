#Maze Solver v1.0
#with go-right-first algorithm

#init
path=[]
node=[]
nc=0 #node count
i=0

#inputs
front,left,right,dest=0,0,0,0
inputs=[[1,0,0,0],[1,0,0,0],[0,1,0,0],[1,0,0,0],[1,0,0,0],[0,1,0,0],[1,0,1,0],[0,0,0,1],[0,0,0,0],[1,0,0,0],[1,1,0,0],[0,0,0,1]]

def forward(): #move 1 step
	pass
	
def goleft(): #turn left & move 1 step
	forward()
	pass
	
def goright(): #turn right & move 1 step
	forward()
	pass

def aboutturn(): #turn around
	forward()
	pass

def stop(): #stop all
	pass

while True:
	front,left,right,dest=inputs[i]
	if (dest!=1):
		if(front==1 and left==0 and right==0):
			forward()
			path.append('F')
		elif(front ==0 and left==1 and right==0):
			goleft()
			path.append('L')
		elif(front==0 and left==0 and right==1):
			goright()
			path.append('R')
		elif(front==1 and left==1 and right==0):
			node.append('F')
			nc=nc+1
			forward()
			path.append('FL')
		elif(front==1 and left==0 and right==1):
			node.append('R')
			nc=nc+1
			goright()
			path.append('RF')
		elif(front==0 and left==1 and right==1):
			node.append('R')
			nc=nc+1
			goright()
			path.append('RL')
		elif(front==1 and left==1 and right==1):
			node.append('R')
			nc=nc+1
			goright()
			path.append('RFL')
		elif(front==0 and left==0 and right==0):
			aboutturn()
			if nc==1:
				path.pop()
		else:
			stop()
			break
	else:
		print 'dest reached'
		stop()
		break
	count=0
	rn=[]
	for j in range(len(path)):
		if len(path[j])>1:
			count=count+1
			rn.append(j)
		if count==2:
			del path[rn[0]+1:rn[1]+1]
			del node[-1]
	i=i+1
	print path,node
for k in path:
	if len(k)>1:
		path[path.index(k)]=path[path.index(k)].replace(node[0],'')
print 'directions:',path
print 'distance:',len(path)
